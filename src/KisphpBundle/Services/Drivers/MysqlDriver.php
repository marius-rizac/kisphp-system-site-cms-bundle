<?php

namespace KisphpBundle\Services\Drivers;

use Kisphp\Utils\Status;
use KisphpBundle\Services\KisphpDatabase;
use KisphpBundle\Services\WidgetContentFactory;
use Shared\Model\ModelRegistry;

class MysqlDriver implements DriverInterface
{
    const CATEGORY_ID_PRESA = 14;

    /**
     * @var \Kisphp\Db\Database
     */
    protected $db;

    /**
     * @param \KisphpBundle\Services\KisphpDatabase $db
     */
    public function __construct(KisphpDatabase $db)
    {
        $this->db = $db->getDb();
    }

    /**
     * @param int $idLayout
     *
     * @return array
     */
    public function getLayoutById($idLayout)
    {
        // @var array $layout
        return $this->db->getRow('SELECT * FROM ' . ModelRegistry::TABLE_CMS_LAYOUT . ' WHERE id = ' . (int) $idLayout);
    }

    /**
     * @return array
     */
    public function getCategoriesForMenu()
    {
        return $this->getCategoriesByParentId(0);
    }

    /**
     * @param int $idLayout
     *
     * @return array
     */
    public function getLayoutRows($idLayout)
    {
        $rows = [];
        $query = $this->db->query('SELECT * FROM ' . ModelRegistry::TABLE_CMS_LAYOUT_ROW . ' WHERE id_layout = ' . (int) $idLayout);
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * @param int $idRow
     *
     * @return array
     */
    public function getRowColumns($idRow)
    {
        $rows = [];
        $query = $this->db->query('SELECT * FROM ' . ModelRegistry::TABLE_CMS_LAYOUT_COLUMN . ' WHERE id_row = ' . (int) $idRow);
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * @param array $widgetData
     * @param string $widgetName
     *
     * @return array
     */
    public function getWidgetContent(array $widgetData, $widgetName)
    {
        $widgetData['content'] = json_decode($widgetData['content'], true);
        if (empty($widgetData['content'])) {
            $widgetData['content'] = [];
        }

        $widgetData['images'] = $this->getMediaFiles($widgetData['id']);

        return [
            'widget' => $widgetData,
            'data' => $this->createWidgetContent($widgetName, $widgetData),
        ];
    }

    /**
     * @param int $categoryId
     *
     * @return array
     */
    public function getArticlesByCategoryId($categoryId)
    {
        $query = $this->db
            ->query(sprintf('SELECT a.id, a.title, a.article_url, aa.filename, aa.directory, a.seo_description
FROM ' . ModelRegistry::TABLE_ARTICLES . ' AS a
LEFT JOIN ' . ModelRegistry::TABLE_MEDIA_FILES . ' AS aa ON (aa.id_object = a.id AND aa.object_type = \'article\')
WHERE a.id_category = %d 
AND a.status = %d
GROUP BY a.id
ORDER BY a.id DESC
', $categoryId, Status::ACTIVE))
        ;

        $articles = [];
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $articles[] = $row;
        }

        return $articles;
    }

    /**
     * @param int $id
     *
     * @return null|array
     */
    public function getColumnWidgets($id)
    {
        $sql = 'SELECT * 
FROM ' . ModelRegistry::TABLE_CMS_LAYOUT_WIDGET . ' 
WHERE id_column = %d 
AND status = %d 
ORDER BY `position` ASC';
        $query = $this->db
            ->query(sprintf($sql, $id, Status::ACTIVE))
        ;

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @return array
     */
    public function getTestimonials()
    {
        $testimonials = $this->db->query('SELECT * FROM ' . ModelRegistry::TABLE_TESTIMONIALS . ' WHERE status = 2 ORDER BY id DESC');

        return $testimonials->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $item_url
     *
     * @return array
     */
    public function getProductCategoryByItemUrl($item_url)
    {
        return $this->db->getRow('SELECT * FROM ' . ModelRegistry::TABLE_PRODUCTS_CATEGORIES . " WHERE item_url = '" . $item_url . "' LIMIT 1");
    }

    /**
     * @param int $idCategory
     *
     * @return array
     */
    public function getProductsByCategory($idCategory)
    {
        return $this->db->query('
SELECT p.id, p.item_url, p.available_stoc, p.price, p.price_old, p.code, p.title, p.seo_title , m.directory, m.filename
FROM ' . ModelRegistry::TABLE_PRODUCTS . ' AS p
LEFT JOIN ' . ModelRegistry::TABLE_MEDIA_FILES . " AS m ON (m.id_object = p.id AND m.object_type = 'product')
WHERE p.id_category = " . (int) $idCategory . ' 
AND p.status = ' . Status::ACTIVE . ' 
GROUP BY p.id
ORDER BY p.id DESC')->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $query
     *
     * @return array|false
     */
    public function searchProductsByQuery($query)
    {
        $sql = $this->db->query('
SELECT p.id, p.item_url, p.available_stoc, p.code, p.price, p.price_old, p.title, p.seo_title , m.directory, m.filename
FROM ' . ModelRegistry::TABLE_PRODUCTS . ' AS p
LEFT JOIN ' . ModelRegistry::TABLE_MEDIA_FILES . " AS m ON (m.id_object = p.id AND m.object_type = 'product')
WHERE p.title LIKE :title 
AND p.status = " . Status::ACTIVE . ' 
GROUP BY p.id
ORDER BY p.id DESC
LIMIT 40', [
            ':title' => '%' . $query . '%',
        ]);

        return $sql->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param int $idProduct
     *
     * @return array
     */
    public function getProductByIdForCatalogPage($idProduct)
    {
        return $this->db->getRow('
SELECT p.id, p.item_url, p.available_stoc, p.price, p.price_old, p.title, p.seo_title, m.directory, m.filename
FROM ' . ModelRegistry::TABLE_PRODUCTS . ' AS p
LEFT JOIN ' . ModelRegistry::TABLE_MEDIA_FILES . " AS m ON (m.id_object = p.id AND m.object_type = 'product')
WHERE p.id = " . (int) $idProduct . ' 
AND p.status = ' . Status::ACTIVE . ' 
GROUP BY p.id
LIMIT 1');
    }

    /**
     * @param int $idProduct
     *
     * @return array
     */
    public function getProductById($idProduct)
    {
        $product = $this->db
            ->getRow('SELECT * 
FROM ' . ModelRegistry::TABLE_PRODUCTS . ' 
WHERE id = :id 
AND status = :status', [
        'id' => $idProduct,
        'status' => Status::ACTIVE,
    ])
        ;

        if (empty($product)) {
            return [];
        }
        $images = $this->db->query('SELECT id, filename, directory 
FROM ' . ModelRegistry::TABLE_MEDIA_FILES . ' 
WHERE object_type = \'product\' 
AND id_object = ' . (int) $product['id']);

        $product['media_files'] = $images->fetchAll(\PDO::FETCH_ASSOC);

        return $product;
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function getProductByIdList(array $ids)
    {
        $query = $this->db
            ->query('SELECT p.* , m.directory, m.filename
FROM ' . ModelRegistry::TABLE_PRODUCTS . ' AS p
LEFT JOIN ' . ModelRegistry::TABLE_MEDIA_FILES . ' AS m ON (m.id_object = p.id AND m.object_type = \'product\')
WHERE FIND_IN_SET(CAST(p.id as char), :ids)
AND p.status = :status
GROUP BY p.id', [
                'ids' => implode(',', $ids),
                'status' => Status::ACTIVE,
            ])
        ;

        $products = [];
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $products[$row['id']] = $row;
        }

        if (empty($products)) {
            return [];
        }

        return $products;
    }

    /**
     * @param string $articleName
     *
     * @return null|array
     */
    public function getArticleByName($articleName)
    {
        $article = $this->db
            ->getRow(sprintf('SELECT * FROM ' . ModelRegistry::TABLE_ARTICLES . " WHERE article_url = '%s' AND status = %d", $articleName, Status::ACTIVE))
        ;

        if (empty($article)) {
            return [];
        }
        $images = $this->db->query('SELECT id, filename, directory FROM ' . ModelRegistry::TABLE_MEDIA_FILES . ' WHERE object_type = \'article\' AND id_object = ' . (int) $article['id']);

        $article['images'] = $images;

        return $article;
    }

    /**
     * @param int $excludeId
     *
     * @return array
     */
    public function getLatestArticles($excludeId)
    {
        $sql = 'SELECT a.id, a.title, a.article_url, aa.filename, aa.directory, a.seo_description
FROM ' . ModelRegistry::TABLE_ARTICLES . ' AS a
LEFT JOIN ' . ModelRegistry::TABLE_MEDIA_FILES . ' AS aa ON (aa.id_object = a.id AND aa.object_type = \'article\')
WHERE a.status = :status
AND a.id != :not_id
GROUP BY a.id
ORDER BY a.id DESC
';

        $query = $this->db
            ->query($sql, [
                'status' => Status::ACTIVE,
                'not_id' => $excludeId,
            ])
        ;

        $articles = [];
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $articles[] = $row;
        }

        return $articles;
    }

    /**
     * @param string $categoryName
     *
     * @return null|array
     */
    public function getCategoryContent($categoryName)
    {
        $category = $this->db->getRow(sprintf('SELECT id, id_parent, `status`, title, item_url, seo_title, seo_keywords, seo_description, background_image, body 
FROM ' . ModelRegistry::TABLE_CATEGORY . ' 
WHERE item_url = \'%s\' 
AND status = %d', $categoryName, Status::ACTIVE));

        if (!$category) {
            return null;
        }

        $category['background_image'] = json_decode($category['background_image'], true);

        return $category;
    }

    /**
     * @param int $categoryId
     *
     * @return array
     */
    public function getCategoryById($categoryId)
    {
        $category = $this->db->getRow(sprintf('SELECT id, id_parent, status, title, item_url, seo_title, seo_keywords, seo_description, body 
FROM ' . ModelRegistry::TABLE_CATEGORY . ' 
WHERE id = %d 
AND status = %d', $categoryId, Status::ACTIVE));

        return $category;
    }

    /**
     * @param string $type
     * @param array $widgetData
     *
     * @return array
     */
    protected function createWidgetContent($type, array $widgetData)
    {
        $type = WidgetContentFactory::create($this, $type);

        return $type->getContent($widgetData);
    }

    /**
     * @param $idWidget
     *
     * @return array
     */
    protected function getMediaFiles($idWidget)
    {
        $imagesQuery = $this->db->query('SELECT * FROM media_files WHERE id_object = ' . (int) $idWidget . " AND object_type = 'widget'");

        $images = [];
        while ($row = $imagesQuery->fetch(\PDO::FETCH_ASSOC)) {
            $images[] = $row;
        }

        return $images;
    }

    /**
     * @param int $id
     *
     * @return array
     */
    protected function getCategoriesByParentId($id)
    {
        $specialities = $this->db
            ->query(sprintf('SELECT * FROM ' . ModelRegistry::TABLE_PRODUCTS_CATEGORIES . ' WHERE id_parent = %d AND status = %d', $id, Status::ACTIVE))
        ;

        $categories = [];
        while ($row = $specialities->fetch(\PDO::FETCH_ASSOC)) {
            $categories[] = [
                'id' => $row['id'],
                'title' => $row['title'],
                'item_url' => $row['item_url'],
                'children' => $this->getCategoriesByParentId($row['id']),
            ];
        }

        return $categories;
    }
}

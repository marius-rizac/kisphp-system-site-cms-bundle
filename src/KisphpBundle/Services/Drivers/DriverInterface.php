<?php

namespace KisphpBundle\Services\Drivers;

interface DriverInterface
{
    /**
     * @param int $idColumn
     *
     * @return array
     */
    public function getColumnWidgets($idColumn);

    /**
     * @param array $widgetData
     * @param string $widgetName
     *
     * @return array
     */
    public function getWidgetContent(array $widgetData, $widgetName);

    /**
     * @param int $idLayout
     *
     * @return array
     */
    public function getLayoutById($idLayout);

    /**
     * @param int $idLayout
     *
     * @return array
     */
    public function getLayoutRows($idLayout);

    /**
     * @param int $idRow
     *
     * @return array
     */
    public function getRowColumns($idRow);
}

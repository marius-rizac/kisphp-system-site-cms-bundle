<?php

namespace KisphpBundle\Controller;

use KisphpBundle\Exceptions\CmsLayoutNotFound;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CmsController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @param int $id
     */
    public function indexAction($id = 0)
    {
        $cms = $this->get('cms.manager')
            ->getLayoutContent($id)
        ;

        return $this->render('KisphpBundle:Cms:index.html.twig', [
            'cms' => $cms,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homepageAction()
    {
        return $this->showLayoutContent(1);
    }

    /**
     * @param int $layoutId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \KisphpBundle\Exceptions\CmsLayoutNotFound
     */
    protected function showLayoutContent($layoutId)
    {
        $cms = $this->get('cms.manager')->getLayoutContent($layoutId);
        if ($cms === null) {
            throw new CmsLayoutNotFound('Layout id = ' . $layoutId);
        }

        return $this->render('KisphpBundle:Cms:index.html.twig', [
            'cms' => $cms,
        ]);
    }
}

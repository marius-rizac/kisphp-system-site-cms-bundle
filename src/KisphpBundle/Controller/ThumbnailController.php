<?php

namespace KisphpBundle\Controller;

use Kisphp\ImageResizer;
use Kisphp\Utils\Files;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ThumbnailController extends Controller
{
    /**
     * @param string $directory
     * @param int $width
     * @param int $height
     * @param string $filename
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function thumbAction($directory, $width, $height, $filename)
    {
        $rootDirectory = dirname($this->get('kernel')->getRootDir());
        $sourceDirectory = Files::inc($rootDirectory . DIRECTORY_SEPARATOR . $this->container->getParameter('upload_directory'));

        $directoryTarget = sprintf(
            '%s/web/thumbs/%s',
            $rootDirectory,
            $directory
        );
        $fileTargetName = sprintf(
            '%dx%d_%s',
            $width,
            $height,
            $filename
        );

        if (is_dir($directoryTarget) === false) {
            mkdir($directoryTarget, 0755, true);
        }

        $sourceFile = Files::inc($sourceDirectory . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $filename);
        $targetFile = Files::inc($directoryTarget . DIRECTORY_SEPARATOR . $fileTargetName);

        if (is_file($sourceFile) === false) {
            $sourceFile = Files::inc($sourceDirectory . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'no-image-available.png');
        }

        $resizer = new ImageResizer();
        $resizer->load($sourceFile);
        $resizer->resize($width, $height, true);
        $resizer->setTarget($targetFile);

        $content = $resizer->display(true);

        return new Response($content);
    }
}
